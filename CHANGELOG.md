# [1.2.0](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/compare/v1.1.0...v1.2.0) (2020-06-09)


### Bug Fixes

* fix a missing zabbix_enable_ldap variable and linting issue ([122cb82](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/commit/122cb82))
* run scripts ([322dd03](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/commit/322dd03))


### Features

* adding ldap ([7bba4b5](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/commit/7bba4b5))

# [1.1.0](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/compare/v1.0.0...v1.1.0) (2020-03-20)


### Features

* adding to disable/enable HA ([f0329f5](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/commit/f0329f5))

# 1.0.0 (2020-03-19)


### Bug Fixes

* Add selinux boolean httpd_can_connect_ldap ([db1e42c](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/commit/db1e42c))
* Import all previous commits from the B network ([2131121](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/commit/2131121))
* Remove the requirement for apache role ([a04f8e6](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/commit/a04f8e6)), closes [#2](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/issues/2)


### Features

* Update gitlab ci to use repoman tempates and generate container ([6baf721](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/commit/6baf721))
